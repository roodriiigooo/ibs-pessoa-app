
CREATE DATABASE banco01;
USE banco01;
CREATE TABLE tb_pessoa
(
    id_pessoa         INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    ds_nome           VARCHAR(100) NOT NULL,
    ds_nascimento 	  VARCHAR(35),
    ds_endereco       VARCHAR(150),
    ds_cpf            VARCHAR(50),
    ds_email          VARCHAR(150),
    ds_fone           VARCHAR(35),
    ds_observacao     VARCHAR(250),
    fl_ativo          bit NOT NULL
);

ALTER DATABASE banco01 CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE tb_pessoa CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;


CREATE DATABASE banco02;
USE banco02;
CREATE TABLE tb_pessoa
(
    id_pessoa         INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    ds_nome           VARCHAR(100) NOT NULL,
    ds_nascimento 	  VARCHAR(35),
    ds_endereco       VARCHAR(150),
    ds_cpf            VARCHAR(50),
    ds_email          VARCHAR(150),
    ds_fone           VARCHAR(35),
    ds_observacao     VARCHAR(250),
    fl_ativo          bit NOT NULL
);

ALTER DATABASE banco02 CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE tb_pessoa CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
