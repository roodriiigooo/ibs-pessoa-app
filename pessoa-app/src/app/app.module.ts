import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { HomeComponent } from './home/home.component';
import { ConsultaComponent } from './pessoa/consulta/consulta.component';
import { ConsultaComponent2 } from './pessoa/consulta/consulta2.component';
import { CadastroComponent} from './pessoa/cadastro/cadastro.component';
import { CadastroComponent2} from './pessoa/cadastro/cadastro.component_2';

import {routing} from './../app.routes';


import {ConfigService} from './services/config.service';
import {ConfigService2} from './services/config2.service';
import {PessoaService} from './services/pessoa.service';
import {PessoaService2} from './services/pessoa2.service';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    ConsultaComponent,
    ConsultaComponent2,
    CadastroComponent,
    CadastroComponent2
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    routing
  ],
  providers: [ConfigService, ConfigService2, PessoaService, PessoaService2],
  bootstrap: [AppComponent]
})
export class AppModule { }
