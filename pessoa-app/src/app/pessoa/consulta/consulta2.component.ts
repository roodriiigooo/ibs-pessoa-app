import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';


import {PessoaService2} from '../../services/pessoa2.service';

import {Pessoa} from '../../services/pessoa';

import {Response} from '../../services/response';

@Component({
    selector: 'app-consulta-pessoa',
    templateUrl: './consulta2.component.html',
    styleUrls:["./consulta2.component.css"]
  })
  export class ConsultaComponent2 implements OnInit {
  
    private pessoas: Pessoa[] = new Array();
    private titulo:string;

    constructor(private pessoaService2: PessoaService2,
                private router: Router){}
  
    ngOnInit() {
      this.titulo = "Registros Cadastrados no Banco02";
      this.pessoaService2.getPessoas().subscribe(res => this.pessoas = res);
    }

    excluir(codigo:number, index:number):void {
      if(confirm("Deseja realmente excluir esse registro?")){
        this.pessoaService2.excluirPessoa(codigo).subscribe(response => {
              let res:Response = <Response>response;
              if(res.codigo == 1){
                alert(res.mensagem);
                this.pessoas.splice(index,1);
              }
              else{
                alert(res.mensagem);
              }
          },
          (erro) => {                    
               alert(erro);
          });        
      }

    }
    editar(codigo:number):void{
        this.router.navigate(['/cadastro-pessoa2',codigo]);
    }
  }