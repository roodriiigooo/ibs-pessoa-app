export class Pessoa {
    
    codigo:number;
	nome:string;				
	ativo:boolean;
	banco : boolean;
	nascimento:string;
	endereco:string;
	cpf:string;
	fone:string;
	email:string;
	observacao:string;
	 
}