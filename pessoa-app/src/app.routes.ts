import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 

import { ConsultaComponent } from './app/pessoa/consulta/consulta.component';
import { ConsultaComponent2 } from './app/pessoa/consulta/consulta2.component'
import { CadastroComponent } from './app/pessoa/cadastro/cadastro.component';
import { CadastroComponent2 } from './app/pessoa/cadastro/cadastro.component_2';

import { HomeComponent } from './app/home/home.component';
 
const appRoutes: Routes = [
    { path: 'home',                    component: HomeComponent },
    { path: '',                        component: HomeComponent },
    { path: 'consulta-pessoa',         component: ConsultaComponent },
    { path: 'consulta2-pessoa',         component: ConsultaComponent2 },
    { path: 'cadastro-pessoa',         component: CadastroComponent },
    { path: 'cadastro-pessoa/:codigo', component: CadastroComponent },
    { path: 'cadastro-pessoa2',         component: CadastroComponent2 },
    { path: 'cadastro-pessoa2/:codigo', component: CadastroComponent2 }

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);